<?php 
     include("conexion.php");	

     session_start();
     $failure = false;
     $success = false;  
     $intentos = 0;
    
      if(isset($_POST["login"]))  
      {  
                
               $usuario = $_POST["usuario"];
               $password =$_POST["password"];
              
               
               $query = "SELECT * FROM usuarios WHERE usuario = :usuario";  
                $statement = $connect->prepare($query);  
                $statement->execute(  
                     array(':usuario'=>$usuario));   
                          
                $res = $statement->fetch(PDO::FETCH_ASSOC);
                $contrasena_cifrada = $res["password"];
               

                $query = "SELECT * FROM usuarios WHERE usuario=:usuario LIMIT 1"; 

                
                if(password_verify($password, $contrasena_cifrada)) {
                    $_SESSION['success'] = '¡Autenticacion exitosa!';
                    $_SESSION['entrada'] = 1;
                   // $_SESSION['usuario'] = "";
                    $_SESSION['intentosFallidos'] = 1;
                    header("location:index.php");  
                    return;
               }
               else{
                    $_SESSION['error'] = '¡Credenciales no válidas!';
                    $intentos = $_SESSION['intentosFallidos'];
                    $intentos ++;
                    $_SESSION['intentosFallidos'] = $intentos; 
                   
                   if($intentos <3){
                    $intentos ++;
                    
                    if($intentos >3){
                         $_SESSION['error'] = '¡Intentos maximos alcanzados!';
                    }
                    $update = 'UPDATE intentos_login SET intentos = :intentos WHERE id =:id';
                    $statement = $connect->prepare($update);
                    $statement->execute(array(':usuario'=>$usuario)); 
                    $res1 = $statement->fetch(PDO::FETCH_ASSOC);
                   }
               }
          }
 ?>  
 <?php
                if ( isset($_SESSION['error']) ){                     
                    echo '<label class="text-danger">'.$_SESSION['error'].'</label>';
                    echo '<label class="text-danger">...Intentos fallidos '.$_SESSION['intentosFallidos'].'</label>';
                    unset($_SESSION['error']);
                }

                if ( isset($_SESSION['success']) ){
                    echo '<label class="text-success">'.$_SESSION['success'].'</label>';
                    unset($_SESSION['success']);
                }
           
?>


<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="favicon.ico">
<title>Inicio de sesion</title>

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="assets/sticky-footer-navbar.css" rel="stylesheet">
</head>

<body>
<header> 
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark"> <a class="navbar-brand" href="#">Comunitec32k</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse" id="navbarCollapse"> 
    </div>
  </nav>
</header>

<!-- Begin page content -->

<div class="container">
  <h3 class="mt-5">Inicio de sesion</h3>
  <hr>
  <div class="row">
    <div class="col-12 col-md-6"> 
      <!-- Contenido -->   
           <form method="post">  
                <div class="form-group">
                    <label for="Usuario">Usuario</label>
                    <input type="text" name="usuario" class="form-control" placeholder="Ingrese usuario" />  
			 </div>
  
                <div class="form-group">
                     <label for="Contraseña">Contraseña</label>
                    <input type="password" name="password" class="form-control" placeholder="Ingrese Contraseña" />  
			 </div>
                      
                <br />  
                     <input type="submit" name="login" class="btn btn-info" value="Iniciar Sesion" />                      
           </form>            
      <!-- Fin Contenido --> 
    </div>
  </div>
  <!-- Fin row -->   
</div>
<!-- Fin container -->

<script src="assets/jquery-1.12.4-jquery.min.js"></script> 
<script src="assets/jquery.validate.min.js"></script> 
<script src="assets/ValidarRegistro.js"></script> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-slim.min.js"><\/script>')</script> 
<script src="assets/js/vendor/popper.min.js"></script> 
<script src="dist/js/bootstrap.min.js"></script>
</body>
</html>
<?php 	
	include('conexion.php');
  session_start();

	if($_SESSION['entrada'] == 0){

		$_SESSION['error'] = 'Favor de iniciar sesion';
		header('Location: login.php'); 
		return;
	}

?>

<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Lista de usuarios</title>

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="assets/sticky-footer-navbar.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(".content").fadeOut(1500);
    },3000);

});
</script>
</head>

<body>
<header> 
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark"> <a class="navbar-brand" href="#">Comunitec32k</a>
  </nav>
</header>

<!-- Begin page content -->

<div class="container">
<?php
    
if(isset($_POST['eliminar'])){

//Se actualiza la tabla
$consulta = "DELETE FROM `usuarios` WHERE `id`=:id";
$sql = $connect-> prepare($consulta);
$sql -> bindParam(':id', $id, PDO::PARAM_INT);
$id=trim($_POST['id']);

$sql->execute();

if($sql->rowCount() > 0)
{
$count = $sql -> rowCount();
echo "<div class='content alert alert-primary' > 
Gracias: $count registro ha sido eliminado  </div>";
}
else{
    echo "<div class='content alert alert-danger'> No se pudo eliminar el registro  </div>";

print_r($sql->errorInfo()); 
}
}
//Cierre de envio de guardado
?>

<?php  
if(isset($_POST['insertar'])){

//Informacion que se envía por el formulario

$usuario=$_POST['usuario'];

$password=$_POST['password'];
$password_cifrada = password_hash($password, PASSWORD_DEFAULT);

$correo=$_POST['correo'];
$telefono=$_POST['telefono'];
$fregis = date('Y-m-d');

//Consulta si el usuario ya existe
$sql= 'SELECT * FROM usuarios WHERE usuario=?';
$stmt = $connect->prepare($sql);
$stmt->execute(array($usuario));
$result = $stmt->fetch();
//Si ya existe, lanza una alerta al usuario
if($result){
  echo "<div class='content alert alert-danger'> ¡Este usuario ya existe!</div>";
  die();
}
//Ahora consulta si el correo existe
$sql= 'SELECT * FROM usuarios WHERE correo=?';
$stmt = $connect->prepare($sql);
$stmt->execute(array($correo));
$result = $stmt->fetch();
//Si ya existe, lanza una alerta al usuario
if($result){
  echo "<div class='content alert alert-danger'> ¡Este correo ya existe!</div>";
  die();
}
//Si el "usuario y correo" NO existen, se continúa con el registro...
$sql="insert into usuarios(usuario,password,correo,telefono,fregis) values(:usuario,:password,:correo,:telefono,:fregis)";
    
$sql = $connect->prepare($sql);  
$sql->bindParam(':usuario',$usuario,PDO::PARAM_STR, 25);
$sql->bindParam(':password',$password_cifrada,PDO::PARAM_STR, 25);
$sql->bindParam(':correo',$correo,PDO::PARAM_STR, 25);
$sql->bindParam(':telefono',$telefono,PDO::PARAM_STR,25);
$sql->bindParam(':fregis',$fregis,PDO::PARAM_STR);
$sql->execute();

$lastInsertId = $connect->lastInsertId();
if($lastInsertId>0){

echo "<div class='content alert alert-primary' >Tu Nombre es : $usuario  Tu contraseña EXPIRA en 2 DIAS </div>";
}
else{
    echo "<div class='content alert alert-danger'> No se pueden agregar datos, comuníquese con el administrador  </div>";

print_r($sql->errorInfo()); 
}
}
//Cierre de envío de guardado
?>

<?php
if(isset($_POST['actualizar'])){
//Informacion que se envía por el formulario
$id=trim($_POST['id']);
$usuario=trim($_POST['usuario']);
$correo=trim($_POST['correo']);
$telefono=trim($_POST['telefono']);
$fregis = date('Y-m-d');

//Consulta si el usuario ya existe
$sql= 'SELECT * FROM usuarios WHERE usuario=?';
$stmt = $connect->prepare($sql);
$stmt->execute(array($usuario));
$result = $stmt->fetch();
//Si ya existe, lanza una alerta al usuario
if($result){
  echo "<div class='content alert alert-danger'> ¡Este usuario ya existe!</div>";
  die();
}
//Ahora consulta si el correo existe
$sql= 'SELECT * FROM usuarios WHERE correo=?';
$stmt = $connect->prepare($sql);
$stmt->execute(array($correo));
$result = $stmt->fetch();
//Si ya existe, lanza una alerta al usuario
if($result){
  echo "<div class='content alert alert-danger'> ¡Este correo ya existe!</div>";
  die();
}
 
//Actualizar tabla
$consulta = "UPDATE usuarios
SET `usuario`= :usuario, `correo` = :correo, `telefono` = :telefono, `fregis` = :fregis
WHERE `id` = :id";
$sql = $connect->prepare($consulta);
$sql->bindParam(':usuario',$usuario,PDO::PARAM_STR, 25);
$sql->bindParam(':correo',$correo,PDO::PARAM_STR, 25);
$sql->bindParam(':telefono',$telefono,PDO::PARAM_STR,25);
$sql->bindParam(':fregis',$fregis,PDO::PARAM_STR);
$sql->bindParam(':id',$id,PDO::PARAM_INT);

$sql->execute();

if($sql->rowCount() > 0)
{
$count = $sql -> rowCount();
echo "<div class='content alert alert-primary' > 

  
Gracias: $count registro ha sido actualizado  </div>";
}
else{
    echo "<div class='content alert alert-danger'> No se pudo actulizar el registro  </div>";

print_r($sql->errorInfo()); 
}
}
//Cierre de envío de guardado
?>
  <h3 class="mt-5">Lista de usuarios</h3>
  <hr>
  <div class="row">

 <!-- Insertar Registros-->
<?php 
if (isset($_POST['formInsertar'])){?>
    <div class="col-12 col-md-12"> 
<form action="" method="POST">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="nombre">Usuario</label>
      <input name="usuario" type="text" class="form-control" placeholder="usuario">
    </div>
    <div class="form-group col-md-6">
      <label for="password">password</label>
      <input name="password" type="text" class="form-control" placeholder="password">
    </div>
    <div class="form-group col-md-6">
      <label for="correo">correo</label>
      <input name="correo" type="email" class="form-control" placeholder="correo">
    </div>
  </div>
<div class="form-row">  
    <div class="form-group col-md-6">
      <label for="telefono">telefono</label>
      <input name="telefono" type="text" class="form-control" placeholder="telefono">
    </div>
</div>
<div class="form-group">
  <button name="insertar" type="submit" class="btn btn-primary  btn-block">Guardar</button>
</div>
</form>
    </div> 
<?php }  ?>
<!-- Fin Insertar Registros-->


<?php 
if (isset($_POST['editar'])){
$id = $_POST['id'];
$sql= "SELECT * FROM usuarios WHERE id = :id"; 
$stmt = $connect->prepare($sql);
$stmt->bindParam(':id', $id, PDO::PARAM_INT); 
$stmt->execute();
$obj = $stmt->fetchObject();
?>

    <div class="col-12 col-md-12"> 

<form role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
    <input value="<?php echo $obj->id;?>" name="id" type="hidden">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="Nombre">Nombre</label>
      <input value="<?php echo $obj->usuario;?>" name="usuario" type="text" class="form-control" placeholder="usuario">
    </div>
    <div class="form-group col-md-6">
      <label for="correo">correo</label>
      <input value="<?php echo $obj->correo;?>" name="correo" type="text" class="form-control" id="edad" placeholder="correo">
    </div>
  </div>
<div class="form-row">  
    <div class="form-group col-md-6">
      <label for="telefono">telefono</label>
      <input value="<?php echo $obj->telefono;?>" name="telefono" type="text" class="form-control" id="profesion" placeholder="telefono">
    </div>
</div>
<div class="form-group">
  <button name="actualizar" type="submit" class="btn btn-primary  btn-block">Actualizar Registro</button>
</div>
</form>
    </div>  
<?php }?>
    <div class="col-12 col-md-12"> 
      <!-- Contenido -->


<div style="float:right; margin-bottom:5px;">

<form action="" method="post"><button class="btn btn-primary" name="formInsertar">Nuevo Usuario</button>  <a href="index.php"><button type="button" class="btn btn-primary">Cancelar</button></a></form></div>

<form class="btn btn-primary" action="CerrarSesion.php" method="post">
		<input type="hidden" name="salir" value="salir">
		<button class="w3-btn w3-green">Salir</button>
  </form>
  
<div class="table-responsive">
<table class="table table-bordered table-striped">
<thead class="thead-dark">
    <th width="18%">usuario</th>
    <th width="22%">correo</th>
    <th width="22%">telefono</th>
    <th width="13%">Fecha registro</th>
    <th width="13%" colspan="2"></th>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM usuarios"; 
$query = $connect -> prepare($sql); 
$query -> execute(); 
$results = $query -> fetchAll(PDO::FETCH_OBJ); 

if($query -> rowCount() > 0)   { 
foreach($results as $result) { 
echo "<tr>
<td>".$result -> usuario."</td>
<td>".$result -> correo."</td>
<td>".$result -> telefono."</td>
<td>".$result -> fregis."</td>
<td>
<form method='POST' action='".$_SERVER['PHP_SELF']."'>
<input type='hidden' name='id' value='".$result -> id."'>
<button name='editar'>Editar</button>
</form>
</td>

<td>
<form  onsubmit=\"return confirm('Realmente desea eliminar el registro?');\" method='POST' action='".$_SERVER['PHP_SELF']."'>
<input type='hidden' name='id' value='".$result -> id."'>
<button name='eliminar'>Eliminar</button>
</form>
</td>
</tr>";

   }
 }
?>
</tbody>
</table>

<!-- Fin container -->


<!-- Bootstrap core JavaScript
    ================================================== --> 
<script src="dist/js/bootstrap.min.js"></script> 
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>